/**
 * @file
 * Workbench notifiertTaskbar javaScript tools.
 */

(function ($) {
    Drupal.taskbar = Drupal.taskbar || {timers: {}};
    Drupal.settings.taskbar = Drupal.settings.taskbar || {};

    Drupal.behaviors.wn_taskbar = {
        attach: function (context, settings) {
            if (Drupal.settings.taskbar.suppress) {
                // Sometimes taskbar_suppress(TRUE) is called too late.
                return;
            }

            $('#taskbar-item-workbench_notifiers_activity', context).addClass('taskbar-item-workbench_notifiers_activity');

            $('#wn_mark_all_read', context).click(function () {
                url = "/ajax/workbench_notifier/mark";
                mark_read(url, true);
            });

            if (nid = getCurrentNodeId()) {
                url = "/ajax/workbench_notifier/mark/" + nid;
                mark_read(url, false);
            }

            function mark_read(url, all) {
                $.ajax({
                    url: url,
                    success: function (response) {
                        if (response.status == 200) {
                            Drupal.taskbar.refresh();
                            if (all) {
                                $('.taskbar-item-workbench_notifiers_activity .item-list li a').each(function () {
                                    $(this).removeClass('wn_unread').addClass('wn_read');
                                });
                            }
                        }
                    }
                });
            }

            function getCurrentNodeId() {
                var $body = $('body.page-node', context);
                if (!$body.length)
                    return false;
                var bodyClasses = $body.attr('class').split(/\s+/);
                for (i in bodyClasses) {
                    var c = bodyClasses[i];
                    if (c.length > 10 && c.substring(0, 10) === "page-node-")
                        return parseInt(c.substring(10), 10);
                }
                return false;
            }

        }
    }
})(jQuery);
