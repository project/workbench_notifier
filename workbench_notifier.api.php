<?php

/**
 * @file
 * API documentation file for Workbench Notifier.
 */

/**
 * Allows modules to alter the workbench notification recipients list.
 *
 * @param array $recipients
 *   Array of objects of recipient users.
 * @param array $moderation
 *   Current workbench moderation info.
 *
 * @see workbench_notifier_workbench_moderation_transition()
 */
function hook_notifier_recipient_list_alter(&$recipients, $moderation) {
  // Alter the recipients list.
}
