<?php
/**
 * @file
 * Administrative forms for Workbench Notifier Module.
 */

/**
 * Administration form to create and delete Notifier transitions.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 *
 * @return form
 *   A ready form array
 */
function workbench_notifier_form($form, &$form_state) {
  // List existing states.
  $types = drupal_map_assoc(workbench_moderation_moderate_node_types());
  $transitions = workbench_moderation_transitions();
  $roles = workbench_notifier_determine_valid_roles();

  if (!$types) {
    drupal_set_message(t('Moderation is not enabled for any content types. Visit
                         the <a href="@url"> content type administration
                         page</a> to enable moderation for one or more types.',
      array('@url' => url('admin/structure/types'))), 'warning');
    $form['#disabled'] = TRUE;
  }
  elseif (!$roles) {
    drupal_set_message(t('Moderation is not enabled for any roles. Visit the
                         <a href="@url"> user permissions page</a> to enable moderation
                         for one or more roles.',
      array('@url' => url('admin/people/permissions', array('fragment' => 'module-workbench_moderation')))), 'warning');
    $form['#disabled'] = TRUE;
  }
  else {
    $form['transitions'] = array(
      '#tree' => TRUE,
    );
  }

  $links = array(
    l(t('Export'), 'admin/config/workbench/notifier/export'),
    l(t('Import'), 'admin/config/workbench/notifier/import'),
  );
  $attributes = array(
    'class' => 'action-links',
  );
  $variables = array(
    'items' => $links,
    'title' => 'Action links',
    'type' => 'ul',
    'attributes' => $attributes,
  );

  $form['action_links'] = array(
    '#markup' => theme_item_list($variables),
  );

  $form['transitions_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Transitions'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['notifiers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notification messages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="workbench-notifier-message-container">',
    '#suffix' => '</div>',
  );

  foreach ($transitions as $transition) {
    $element = array();

    $element['transition'] = array(
      '#type' => 'value',
      '#value' => $transition,
    );
    $element['from_name'] = array(
      '#markup' => check_plain(workbench_moderation_state_label($transition->from_name)),
    );
    $element['to_name'] = array(
      '#markup' => check_plain(workbench_moderation_state_label($transition->to_name)),
    );

    $workbench_notify = workbench_notifier_get($transition);
    $transition_label = $transition->from_name . '_to_' . $transition->to_name;
    foreach ($roles as $rid => $role) {
      $transition_default_value = isset($workbench_notify[$transition_label]) ? in_array($rid, $workbench_notify[$transition_label]->rids) : FALSE;
      $element[$role]['notify'] = array(
        '#type' => 'checkbox',
        '#title' => check_plain(t('Notify')),
        '#default_value' => $transition_default_value,
        '#prefix' => '<div class="workbench-notifier-notify-container">',
        '#suffix' => '</div>',
        '#attributes' => array('class' => array('workbench-notifier-notify-checkbox')),
      );
    }
    $form['transitions'][] = $element;

    $label = ucwords($transition->from_name . t('To') . $transition->to_name);
    $message = isset($workbench_notify[$transition_label]) ? $workbench_notify[$transition_label]->message : '';
    $form['notifiers']['message'][$transition_label] = array(
      '#type' => 'textfield',
      '#default_value' => $message,
      '#title' => $label,
      '#size' => 60,
    );
  }

  if (module_exists('token')) {
    module_load_include('inc', 'token', 'token.pages');
    $form['notifiers']['token_set'] = array(
      '#type' => 'fieldset',
      '#title' => t('Available Tokens'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $token_tree_theme = 'token_tree';
    $form['notifiers']['token_set']['tokens'] = array(
      '#theme' => $token_tree_theme,
      '#token_types' => array('node', 'user'),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  );

  return $form;
}

/**
 * Form submit handler for notifier transitions.
 *
 * Adds or deletes notifier transitions depending on user input.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 */
function workbench_notifier_form_submit($form, &$form_state) {
  $roles = workbench_notifier_determine_valid_roles();
  foreach ($form_state['values']['transitions'] as $transition) {
    $notify_rids = array();
    $discard_rids = array();
    $transition_label = $transition['transition']->from_name . '_to_' . $transition['transition']->to_name;
    $message = $form_state['values'][$transition_label];
    foreach ($roles as $rid => $role) {
      if ($transition[$role]['notify']) {
        $notify_rids[] = $rid;
      }
      else {
        $discard_rids[] = $rid;
      }
    }
    if (!empty($discard_rids)) {
      workbench_notifier_delete($transition['transition'], $discard_rids);
    }
    workbench_notifier_save($transition['transition'], $notify_rids, $message);
  }
  drupal_set_message(t('Settings have been updated'));
}

/**
 * Transforms the notifier transitions administration form into a table.
 *
 * @param array $variables
 *   The form array to render into a table.
 *
 * @return output
 *   A rendered form in table structure
 */
function theme_workbench_notifier_form($variables) {
  $form = $variables['form'];

  $header = array(
    t('From'),
    '',
    t('To'),
  );

  $roles = workbench_notifier_determine_valid_roles();
  foreach ($roles as $rid => $role) {
    $header[] = t("@role", array('@role' => ucwords($role)));
  }

  $rows = array();
  foreach (element_children($form['transitions']) as $key) {
    $element = &$form['transitions'][$key];
    if (!isset($element['#markup'])) {
      $row = array('data' => array());
      $row['data']['from'] = drupal_render($element['from_name']);
      $row['data'][] = '--&gt;';
      $row['data']['to'] = drupal_render($element['to_name']);
      foreach ($roles as $rid => $role) {
        $data = drupal_render($element[$role]['notify']);
        $data .= drupal_render($element[$role]['auto_notify']);
        $row['data'][$role] = $data;
      }
      $rows[] = $row;
    }
  }

  $table = theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'class' => array('width-auto'),
        'id' => array('workbench-notifier-table'),
      ),
    )
  );

  $form['transitions_container']['#value'] = $table;
  $output = drupal_render_children($form);
  return $output;
}

/**
 * Function to export notifier settings.
 */
function workbench_notifier_export() {
  $export = workbench_notifier_get();

  $export_data['element'] = array(
    '#value' => json_encode($export),
    '#rows' => 10,
    '#attributes' => array(
      'onclick' => 'this.focus();this.select()',
      'readonly' => 'readonly',
    ),
  );

  $page['export_data'] = array(
    '#markup' => theme_textarea($export_data),
  );

  return $page;
}

/**
 * Function to import notifier settings.
 */
function workbench_notifier_import_form($form, &$form_state) {
  $form['import_data'] = array(
    '#title' => t('Paste notifier settings'),
    '#type' => 'textarea',
    '#default_value' => '',
    '#rows' => 10,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Import',
  );

  return $form;
}

/**
 * Function to validate import notifier settings.
 */
function workbench_notifier_import_form_validate($form, &$form_state) {
  $import_data = json_decode($form_state['values']['import_data']);
  $form_state['import_data'] = $import_data;
  if (!is_object($import_data) && !is_array($import_data)) {
    form_set_error('import_data', t('Invalid settings to import.'));
  }
  else {
    foreach ($import_data as $notifier) {
      if (!is_object($notifier)) {
        form_set_error('import_data', t('Invalid settings to import.'));
        break;
      }
      elseif (!isset($notifier->from_name) || !isset($notifier->to_name) || !isset($notifier->message) || !isset($notifier->rids) || !is_array($notifier->rids)) {
        form_set_error('import_data', t('Invalid settings to import.'));
        break;
      }
    }
  }
}

/**
 * Submission handler for importing notifier settings.
 */
function workbench_notifier_import_form_submit($form, &$form_state) {
  $import_data = $form_state['import_data'];
  foreach ($import_data as $notifier) {
    workbench_notifier_save($notifier);
  }
  drupal_set_message(t('Notifier settings imported successfully'), 'status', FALSE);
}
