/**
 * @file
 * README file for Workbench Notifier.
 */

Workbench Notifier Module

CONTENTS
--------

1.  Introduction
2.  Requirements
3.  Configuration

1.  Introduction
----------------

Workbench Notifier

1.1  Concepts
-------------

Extends Workbench Moderation by adding the ability to get push notification to
specific transitions to those specific role based user(s). Based on transitions,
the admin can configure each notification's message. Push notification depends
on taskbar activity module.

2.  Requirements
----------------

Workbench Notifier requires:
- Workbench Moderation (and dependencies)
- Token
- Taskbar
- Taskbar Activity

3.  Configuration
-----------------

*  Download and install the module.
*  Go to 'admin/config/workbench/notifier' and enable notifier for each
   transition based on user roles.
*  Notification message also can be defined with token support for each
   transition. If message is not defined for any transition then default format
   will be rendered, default format is 'Content [node:title] is been moderated
   from <previous_state> to <current_state>'.
*  Set correct permission access for taskbar for user roles having access to
   content moderation.
