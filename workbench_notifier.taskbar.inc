<?php
/**
 * @file
 * Simple source for Taskbar activity.
 */

/**
 * Implements hook_taskbar_activity_source().
 */
function workbench_notifier_taskbar_activity_source() {
  return array(
    array(
      'name' => 'workbench_notifiers_activity',
      'description' => 'Workbench notifier source',
      'callbacks' => array(
        'count' => 'workbench_notifier_taskbar_activity_count',
        'list' => 'workbench_notifier_taskbar_activity_list',
        'mark' => 'workbench_notifier_taskbar_activity_mark',
      ),
    ),
  );
}

/**
 * Taskbar activity source count callback.
 *
 * @param bool $unread
 *   Get notification count of unread.
 *
 * @return int $count
 *   Notification count.
 */
function workbench_notifier_taskbar_activity_count($unread = FALSE) {
  global $user;
  $status = $unread ? 0 : 1;
  $query = db_select('workbench_notifier_update', 'u')
    ->fields('u', array('wnuid'))
    ->condition('recipient', $user->uid)
    ->condition('status', $status)
    ->execute();
  $count = $query->rowCount();

  return $count;
}

/**
 * Taskbar activity source list callback.
 *
 * @param int $count
 *   Count of notifications to be returned.
 * @param int $start
 *   Offset.
 *
 * @return array $list
 *   List of notifications.
 */
function workbench_notifier_taskbar_activity_list($count = 20, $start = 0) {
  global $user;

  $list = array();

  $query = db_select('workbench_notifier_update', 'u');
  $query->join('workbench_moderation_node_history', 'h', "u.hid = h.hid and u.recipient = $user->uid");
  $query->join('workbench_notifiers', 'nf', 'h.from_state = nf.from_name and h.state = nf.to_name');
  $query->join('node', 'n', 'h.nid = n.nid');
  $query->fields('nf', array('message'))
    ->fields('h', array('nid', 'from_state', 'state'))
    ->fields('n', array('title'))
    ->fields('u', array('status'))
    ->orderBy('u.hid', 'ASC')
    ->range($start, $count);

  $notifications = $query->execute()->fetchAll();

  foreach ($notifications as $notification) {
    if ($notification->status) {
      $wn_class = 'wn_read';
    }
    else {
      $wn_class = 'wn_unread';
    }
    if (empty($notification->message) || $notification->message == '') {
      $from_state = workbench_moderation_state_label($notification->from_state);
      $to_state = workbench_moderation_state_label($notification->state);
      $message = 'Content ' . $notification->title . ' is been moderated from ' . $from_state . ' to ' . $to_state;
    }
    else {
      $node = node_load($notification->nid);
      $message = token_replace($notification->message, array('node' => $node, 'user' => $user));
    }
    $attributes = array(
      'class' => array($wn_class),
      'data-nid' => $notification->nid,
    );
    $list[] = l(t($message), "node/$notification->nid", array('attributes' => $attributes));
  }

  if (empty($list)) {
    $list[] = 'Empty list.';
  }
  else {
    array_unshift($list, '<span id="wn_mark_all_read">Mark all as read</span>');
  }

  return $list;
}

/**
 * Taskbar activity source mark callback.
 */
function workbench_notifier_taskbar_activity_mark() {
  // We can't mark the notification as read here,
  // because we need it to be marked separately for each.
}

/**
 * Custom workbench notifier activity mark callback.
 *
 * @param int $nid
 *   Node id based on which notification need to be marked as read.
 *
 * @return json $response
 *   Ajax json response status code.
 */
function workbench_notifier_taskbar_activity_ajax_mark($nid = NULL) {
  $response = array('status' => 200);
  try {
    global $user;

    $query = db_update('workbench_notifier_update')
      ->fields(array('status' => 1))
      ->condition('recipient', $user->uid)
      ->condition('status', 0);

    if (is_numeric($nid)) {
      $hid = db_select('workbench_moderation_node_history', 'h')
        ->fields('h', array('hid'))
        ->condition('nid', $nid)
        ->condition('is_current', 1)
        ->execute()->fetchCol();

      $query->condition('hid', $hid);
    }
    $query->execute();
  }
  catch (Exception $e) {
    $response['status'] = 500;
    $response['error'] = $e->getMessage();
  }
  drupal_json_output($response);
  drupal_exit();
}
